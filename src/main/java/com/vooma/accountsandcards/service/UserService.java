package com.vooma.accountsandcards.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.vooma.accountsandcards.entity.User;
import com.vooma.accountsandcards.forms.UserRegistrationForm;
import com.vooma.accountsandcards.repo.UserRepository;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private PasswordEncoder encoder;

    public User findByID(long id) {
        Optional<User> user = userRepo.findById(id);
        if (user.isPresent()) {
            return user.get();
        }
        return null;
    }

    public User findByEmail(String email) {
        Optional<User> user = userRepo.findByEmail(email);
        if (user.isPresent()) {
            return user.get();
        }
        return null;
    }

    public List<User> filterUsers() {
        return userRepo.findAll(Sort.by("id").descending());
    }

    public User save(UserRegistrationForm form) {
        Optional<User> existingUser = userRepo.findByEmail(form.getEmail());
        if (existingUser.isPresent()) {
            return existingUser.get();
        } else {
            User user = new User();
            user.setEmail(form.getEmail());
            user.setFirst_name(form.getFirst_name());
            user.setLast_name(form.getLast_name());
            user.setPassword(encoder.encode(form.getPassword()));
            user.setRoles(form.getRoles());
            return userRepo.save(user);
        }
    }

    public User updateUser(User user, long userID) {
        User existingUser = userRepo.findById(userID).get();
        if (existingUser == null) {
            return null;
        }

        if (existingUser.getEmail() == null) {
            existingUser.setEmail(user.getEmail());
        }

        existingUser.setFirst_name(user.getFirst_name());
        existingUser.setLast_name(user.getLast_name());

        return userRepo.save(existingUser);
    }

}
