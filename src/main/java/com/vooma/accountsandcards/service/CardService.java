package com.vooma.accountsandcards.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vooma.accountsandcards.entity.Account;
import com.vooma.accountsandcards.entity.Card;
import com.vooma.accountsandcards.model.CardList;
import com.vooma.accountsandcards.model.Pagination;
import com.vooma.accountsandcards.repo.AccountRepository;
import com.vooma.accountsandcards.repo.CardRepository;

@Service
public class CardService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private CardRepository cardRepository;

    public Card deleteCard(Long id) {
        Card existingCard = cardRepository.findById(id).get();
        if (existingCard == null) {
            return null;
        }

        Date timeNow = new Date();
        existingCard.setDeletedAt(timeNow);
        cardRepository.save(existingCard);

        return existingCard;
    }

    public Card getByID(long id) {
        return cardRepository.findById(id).get();
    }

    public CardList filterCards() {
        CardList cardList = new CardList();
        List<Card> Cards = cardRepository.findAllActiveCards();
        cardList.setCards(Cards);

        Pagination pagination = new Pagination(1, 20);
        Long CardsCount = cardRepository.countActiveCards();
        pagination.setCount(CardsCount);
        cardList.setPagination(pagination);
        return cardList;
    }

    public Card save(Card card) {

        // Validate account id
        Account existingAccount = accountRepository.findById(card.getAccountID()).get();
        if (existingAccount == null) {
            return null;
        }

        return cardRepository.save(card);
    }

    public Card updateCard(Card card, long cardID) {
        Card existingCard = cardRepository.findById(cardID).get();
        if (existingCard == null) {
            return null;
        }

        if (card.getCardAlias() != null) {
            existingCard.setCardAlias(card.getCardAlias());
        }

        Date timeNow = new Date();
        existingCard.setUpdatedAt(timeNow);

        return cardRepository.save(existingCard);
    }

}
