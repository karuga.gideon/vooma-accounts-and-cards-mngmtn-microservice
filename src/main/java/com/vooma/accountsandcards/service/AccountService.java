package com.vooma.accountsandcards.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vooma.accountsandcards.entity.Account;
import com.vooma.accountsandcards.model.AccountList;
import com.vooma.accountsandcards.model.Pagination;
import com.vooma.accountsandcards.repo.AccountRepository;

@Service
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    public Account deleteAccount(Long id) {
        Account existingAccount = accountRepository.findById(id).get();
        if (existingAccount == null) {
            return null;
        }

        Date timeNow = new Date();
        existingAccount.setDeletedAt(timeNow);
        accountRepository.save(existingAccount);

        return existingAccount;
    }

    public Account getByID(long id) {
        return accountRepository.findById(id).get();
    }

    public AccountList filterAccounts() {
        AccountList AccountList = new AccountList();
        List<Account> Accounts = accountRepository.findAllActiveAccounts();
        AccountList.setAccounts(Accounts);

        Pagination pagination = new Pagination(1, 20);
        Long AccountsCount = accountRepository.countActiveAccounts();
        pagination.setCount(AccountsCount);
        AccountList.setPagination(pagination);
        return AccountList;
    }

    public Account save(Account account) {
        return accountRepository.save(account);
    }

}
