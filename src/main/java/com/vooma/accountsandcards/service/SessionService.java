package com.vooma.accountsandcards.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.vooma.accountsandcards.entity.Session;
import com.vooma.accountsandcards.repo.SessionRepository;

@Service
public class SessionService {

    @Autowired
    private SessionRepository sessionRepository;

    public Session getByID(long id) {
        return sessionRepository.findById(id).get();
    }

    public Session findBySessionToken(String sessionToken) {
        return sessionRepository.findBySessionToken(sessionToken);
    }

    public List<Session> filterSessions() {
        return sessionRepository.findAll(Sort.by("id").descending());
    }

    public Session save(Session session) {
        Session existingSession = sessionRepository.findBySessionToken(session.getSessionToken());
        if (existingSession == null) {
            return sessionRepository.save(session);
        }

        return existingSession;
    }

    public Session updateClient(Session session, long sessionID) {
        Session existingClient = sessionRepository.findById(sessionID).get();
        if (existingClient == null) {
            return null;
        }

        LocalDateTime localDateTime = LocalDateTime.now();
        existingClient.setLastRefreshedAt(localDateTime);

        return sessionRepository.save(existingClient);
    }
}
