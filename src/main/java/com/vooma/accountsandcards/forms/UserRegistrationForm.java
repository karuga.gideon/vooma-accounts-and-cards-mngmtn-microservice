package com.vooma.accountsandcards.forms;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserRegistrationForm {

    private String email;
    private String first_name;
    private String last_name;
    private String password;
    private String roles;

}
