package com.vooma.accountsandcards.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.vooma.accountsandcards.entity.Account;

public interface AccountRepository extends JpaRepository<Account, Long> {

    @Query("SELECT c FROM Account c WHERE c.deletedAt IS NULL ORDER BY id DESC")
    List<Account> findAllActiveAccounts();

    @Query("SELECT COUNT(*) FROM Account c WHERE c.deletedAt IS NULL")
    Long countActiveAccounts();
}
