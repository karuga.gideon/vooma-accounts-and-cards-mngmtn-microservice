package com.vooma.accountsandcards.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vooma.accountsandcards.entity.Session;

public interface SessionRepository extends JpaRepository<Session, Long> {

    Session findBySessionToken(String sessionToken);
}
