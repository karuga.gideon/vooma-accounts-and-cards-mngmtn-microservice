package com.vooma.accountsandcards.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vooma.accountsandcards.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByEmail(String email);
}
