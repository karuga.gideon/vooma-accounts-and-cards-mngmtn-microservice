package com.vooma.accountsandcards.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.vooma.accountsandcards.entity.Card;

public interface CardRepository extends JpaRepository<Card, Long> {

    @Query("SELECT c FROM Card c WHERE c.deletedAt IS NULL ORDER BY id DESC")
    List<Card> findAllActiveCards();

    @Query("SELECT COUNT(*) FROM Card c WHERE c.deletedAt IS NULL")
    Long countActiveCards();
}
