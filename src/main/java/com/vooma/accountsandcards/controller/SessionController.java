package com.vooma.accountsandcards.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vooma.accountsandcards.forms.UserLoginForm;
import com.vooma.accountsandcards.entity.Session;
import com.vooma.accountsandcards.entity.User;
import com.vooma.accountsandcards.service.JwtService;
import com.vooma.accountsandcards.service.SessionService;
import com.vooma.accountsandcards.service.UserService;
import com.vooma.accountsandcards.utils.HttpUtils;

import jakarta.servlet.http.HttpServletRequest;

@RestController
public class SessionController {

    @Autowired
    private JwtService jwtService;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private SessionService sessionService;

    @Autowired
    private UserService userService;

    private static final Logger logger = LoggerFactory.getLogger(SessionController.class);

    @PostMapping("/sessions")
    public ResponseEntity<Object> authenticateAndGetToken(
            HttpServletRequest request,
            @RequestBody UserLoginForm form) {

        HttpHeaders responseHeaders = new HttpHeaders();
        Map<String, String> errorResponse = new HashMap<>();
        errorResponse.put("error", "invalid credentials");

        User user = userService.findByEmail(form.getEmail());
        if (user == null) {
            logger.info("invalid email login attempt for {}", form.getEmail());
            return new ResponseEntity<>(errorResponse, responseHeaders, HttpStatus.UNAUTHORIZED);
        }

        if (encoder.matches(form.getPassword(), user.getPassword())) {
            String token = jwtService.generateToken(form.getEmail());

            Session session = new Session();
            session.setUserID(user.getId());
            session.setSessionToken(token);
            session.setApplicationType("admin");
            session.setIpAddress(HttpUtils.getRequestIP(request));
            session.setUserAgent(request.getHeader("USER-AGENT"));
            sessionService.save(session);

            responseHeaders.set("X-VOOMA-TOKEN", token);
            return new ResponseEntity<>(user, responseHeaders, HttpStatus.OK);

        } else {
            logger.info("invalid pwd login attempt for {}", form.getEmail());
            return new ResponseEntity<>(errorResponse, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/sessions")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public List<Session> filterSessions() {
        return sessionService.filterSessions();
    }

    @GetMapping("/sessions/{id}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public Session findSessionByID(@PathVariable int id) {
        return sessionService.getByID(id);
    }
}
