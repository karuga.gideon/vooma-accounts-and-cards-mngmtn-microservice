package com.vooma.accountsandcards.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vooma.accountsandcards.entity.Account;
import com.vooma.accountsandcards.entity.Card;
import com.vooma.accountsandcards.model.CardList;
import com.vooma.accountsandcards.service.CardService;

@RestController
@RequestMapping("/cards")
public class CardController {

    @Autowired
    private CardService cardService;

    @PostMapping("")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public Card createCard(@RequestBody Card card) {
        return cardService.save(card);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public Card deleteCardByID(@PathVariable Long id) {
        return cardService.deleteCard(id);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public Card findCardByID(@PathVariable int id) {
        return cardService.getByID(id);
    }

    @GetMapping("")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public CardList filterCards() {
        return cardService.filterCards();
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public Card updateCard(@RequestBody Card card, @PathVariable Long id) {
        return cardService.updateCard(card, id);
    }
}
