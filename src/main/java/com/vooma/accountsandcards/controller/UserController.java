package com.vooma.accountsandcards.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vooma.accountsandcards.entity.User;
import com.vooma.accountsandcards.forms.UserRegistrationForm;
import com.vooma.accountsandcards.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("")
    public User createUser(@RequestBody UserRegistrationForm form) {
        return userService.save(form);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public User findUserByID(@PathVariable long id) {
        User user = userService.findByID(id);
        if (user != null) {
            return user;
        } else {
            return null;
        }
    }

    @GetMapping("/by_email/{email}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public User findUserByEmail(@PathVariable String email) {
        return userService.findByEmail(email);
    }

    @GetMapping("")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public List<User> filterUsers() {
        return userService.filterUsers();
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public User updateUser(@RequestBody User user, @PathVariable int id) {
        return userService.updateUser(user, id);
    }
}
