package com.vooma.accountsandcards;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccountsAndCardsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountsAndCardsApplication.class, args);
	}

}
