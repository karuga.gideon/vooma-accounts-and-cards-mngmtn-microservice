package com.vooma.accountsandcards.filter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.vooma.accountsandcards.entity.User;
import com.vooma.accountsandcards.entity.UserInfoDetails;
import com.vooma.accountsandcards.repo.UserRepository;
import com.vooma.accountsandcards.service.JwtService;

import java.io.IOException;
import java.util.Optional;

// This class helps us to validate the generated jwt token
@Component
public class JwtAuthFilter extends OncePerRequestFilter {

    private static final Logger logger = LoggerFactory.getLogger(JwtAuthFilter.class);

    @Autowired
    private JwtService jwtService;

    @Autowired
    private UserRepository userRepo;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        String authHeader = request.getHeader("Authorization");
        String token = null;
        String email = null;

        if (authHeader != null && authHeader.length() > 50 && authHeader.startsWith("Bearer ")) {
            token = authHeader.substring(7);

            if (jwtService.isTokenExpired(token)) {
                logger.info("invalid token or token expired");
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                new UsernameNotFoundException("invalid token or token expired");
                return;
            }

            email = jwtService.extractEmail(token);
        }

        if (email != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            Optional<User> user = userRepo.findByEmail(email);
            if (user == null) {
                logger.info("invalid email login attempt for {}", email);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                new UsernameNotFoundException("invalid authentication");
                return;
            }

            UserDetails userDetails = user.map(UserInfoDetails::new)
                    .orElseThrow(() -> new UsernameNotFoundException("User not found "));

            if (jwtService.validateToken(token, userDetails)) {
                UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(userDetails,
                        null, userDetails.getAuthorities());
                authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authToken);
            } else {
                logger.info("invalid token or token expired");
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                new UsernameNotFoundException("invalid token or token expired");
                return;
            }
        }
        filterChain.doFilter(request, response);
    }
}
