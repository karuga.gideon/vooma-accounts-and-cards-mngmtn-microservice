package com.vooma.accountsandcards.model;

import java.util.List;

import com.vooma.accountsandcards.entity.Account;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AccountList {

    private List<Account> Accounts;
    private Pagination pagination;
}
