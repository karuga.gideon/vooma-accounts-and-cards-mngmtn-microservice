package com.vooma.accountsandcards.model;

import java.util.List;

import com.vooma.accountsandcards.entity.Card;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CardList {

    private List<Card> Cards;
    private Pagination pagination;
}
