package com.vooma.accountsandcards.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Pagination {

    public Pagination(int page, int per) {
        this.page = page;
        this.per = per;
    }

    private int page;
    private int per;
    private Long count;
}
