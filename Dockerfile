FROM maven:3.8.3-openjdk-17 AS build

COPY src /home/app/src
COPY pom.xml /home/app 

RUN mvn -f /home/app/pom.xml clean package

EXPOSE 8052

ENV SPRING_PROFILES_ACTIVE=prod

ENTRYPOINT ["java","-Dspring.config.activate.on-profile=prod", "-jar","/home/app/target/vooma-accounts-cards-0.0.1.jar"]
