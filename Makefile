down: 
	docker-compose down

package: 
	mvn clean package
	
server: 
	mvn spring-boot:run -Dspring-boot.run.profiles=prod

up: 
	docker-compose -f docker-compose.yml up --build --detach	
