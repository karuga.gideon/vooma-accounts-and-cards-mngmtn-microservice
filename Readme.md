## Overview

The account and card management feature will allow for the management of credit cards linked
to accounts within the banking system..
It will involve management of the following:

1. Accounts - Creation, Retrieval, Update and Deletion.
2. Cards - Creation, Retrieval, Update and Deletion.
   Each card will be linked to one account, and clients belonging to an account can retrieve the
   cards belonging to that account.

## Getting started

This project uses postgres database, you will need to install it and create a database called vooma_accounts_cards.

Once created, start the application by running the following command;

make server

The database tables will be created automatically.

Proceed to create a user account with the following endpoint and sample details;

POST http://localhost:8052/users

{
"email":"john.doe@gmail.com",
"first_name":"John",
"last_name":"Doe",
"password":"1234",
"roles": "ROLE_USER,ROLE_ADMIN,SUPERUSER"
}

Login using the following endpoint;

POST http://localhost:8052/sessions

{
"email":"john.doe@gmail.com",
"password":"1234"
}

After a successful login, a token will be provided under the following response header;

X-VOOMA-TOKEN

Copy it and set it as a bearer token for use in subsequent authorized requests.

Create some test accounts using the following endpoint;

POST http://localhost:8052/accounts

{
"iban":"KCBLKENX",
"bankCode":"KCBLKENX",
"customerID": 10008876001
}

Access created accounts via the following endpoint;

GET http://localhost:8052/accounts

Create cards using the following endpoint;

POST http://localhost:8052/accounts

{
"accountID": 1,
"cardAlias":"JOHN DOE",
"cardType": "virtual"
}

Access created cards via the following endpoint;

POST http://localhost:8052/cards
